import React, { Component } from "react";
import ReactGA from "react-ga";
import $ from "jquery";

import Header from "./Components/Header";
import Footer from "./Components/Footer";
import About from "./Components/About";
import Resume from "./Components/Resume";
import Contact from "./Components/Contact";
import FunFacts from "./Components/FunFacts";
import Porfolio from "./Components/Porfolio";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resumeData: {},
      user: {
        presentation: {
          name: "Belen",
          occupation: "Fullstack student",
          description: "hola",
          city: "Tucuman",
          profilepic: "urldelaimagen",
          bio: "bio",
          address: "adress",
          phone: "812746",
          email: "betutub@gmail.com",
          social: [
            {
              className: "fa fa-twitter",
              name: "twitter",
              url: "url",
            },
            {
              className: "fa fa-google-plus",
              name: "google",
              url: "url",
            },
            {
              className: "fa fa-skype",
              name: "skype",
              url: "url",
            },
          ],
        },

        resume: {
          education: [
            {
              school: "university",
              degree: "degre",
              graduated: "graduate",
              description: "description",
            },
            {
              school: "university",
              degree: "degre",
              graduated: "graduate",
              description: "description",
            },
          ],
          work: [
            {
              company: "company",
              title: "title",
              years: 1,
              description: "desciption",
            },
            {
              company: "company",
              title: "title",
              years: 1,
              description: "desciption",
            },
          ],
          projects: [
            {
              image: "url",
              title: "titulo",
              url: "urlpagina",
              category: "category",
            },
          ],
          skills: [{ name: "react", level: "20%" }],
        },
        funfacts: [
          {
            text: "asdasd",
            url: "url",
          },
          {
            text: "asdasd",
            url: "url",
          },
        ],
      },
    };

    ReactGA.initialize("UA-110570651-1");
    ReactGA.pageview(window.location.pathname);
  }

  render() {
    const { presentation,resume} = this.state.user;
    return (
      <div className="App">
        <Header data={presentation} />
        <About data={presentation} />
        <Resume data={resume} />
        <Contact data={presentation} />
        <Footer data={presentation} />
      </div>
    );
  }
}

export default App;
